<?php

/**
 * @file
 * Include for admin page of mobile switch redirects module.
 */

function mobile_switch_redirects_configuration_form(){
  $form = array();

  $form['mobile_switch_redirects_desktop_site_redirects'] = array(
    '#type' => 'textarea',
    '#title' => t('Desktop Site Redirects'),
    '#description' => t('These redirects only apply when the visitor is visiting using a desktop.
     On each line define the requested path and the redirect path, separated by a \'|\'.'),
    '#default_value' => variable_get('mobile_switch_redirects_desktop_site_redirects',''),
  );

  $form['mobile_switch_redirects_mobile_site_redirects'] = array(
    '#type' => 'textarea',
    '#title' => t('Mobile Site Redirects'),
    '#description' => t('These redirects only apply when the visitor is visiting using a mobile device.
     On each line define the requested path and the redirect path, separated by a \'|\'.'),
    '#default_value' => variable_get('mobile_switch_redirects_mobile_site_redirects',''),
  );

  return system_settings_form($form);
}